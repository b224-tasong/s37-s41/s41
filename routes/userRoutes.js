const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");


// Routes

// Route for checking email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)) 
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for user log in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// router.post("/details", (req, res) => {
// 	userController.getProfile({id: req.body.id}).then(resultFromController => res.send(resultFromController))
// });

// router.post("/details", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);
// 	console.log(userData)
// 	console.log(req.headers.authorization);

// 	userController.getProfile({userid: req.body.id}).then(resultFromController => res.send(resultFromController))
// });


router.post("/details", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);
	console.log(data)
	console.log(req.headers.authorization);

	userController.getProfile({id: data.id}).then(resultFromController => res.send(resultFromController))
});


// router.post("/details", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);
// 	console.log(userData)
// 	console.log(req.headers.authorization);
	
// 	userController.getProfile({reqBody: req.body.id}).then(resultFromController => res.send(resultFromController))
// });



// Route for enrolling an authenticated user

// router.post("/enroll", (req, res) => {
// 	let data = {
// 		userId: req.body.userId,
// 		courseId: req.body.courseId
// 	}
// 	userController.enroll(data).then(resultFromController => res.send(resultFromController))
// })



// ======================Activity 42=========================

// router.post("/enroll", auth.verify, (req, res) => {

// 	// let data = {
// 	// 	userId: req.body.userId,
// 	// 	courseId: req.body.courseId
// 	// }
// 	const userData = auth.decode(req.headers.authorization);
// 	let data = {
// 			userId: req.body.userId,
// 			courseId: req.body.courseId
// 		}
// 	console.log(userData.isAdmin)

// 	if (userData.isAdmin) {
// 		res.send({auth: "You are not a student"})
// 	}else{
// 		userController.enroll(data).then(resultFromController => res.send(resultFromController))
// 	}
	
// })



router.post("/enroll", auth.verify, (req, res) => {


	const userData = auth.decode(req.headers.authorization);
	let data = {
			userId: req.body.userId,
			courseId: req.body.courseId
		}

	
	if (userData.isAdmin) {
		res.send({auth: "You are not a student"})
	}else{
		userController.enroll(data).then(resultFromController => res.send(resultFromController))
	}
	
})



module.exports = router;